﻿namespace bed_exit_alarm_one_window_for_test
{
    partial class bed_exit_alarm_system
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(bed_exit_alarm_system));
            this.bedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.beddbDataSet3 = new bed_exit_alarm_one_window_for_test.beddbDataSet3();
            this.beddbDataSet2 = new bed_exit_alarm_one_window_for_test.beddbDataSet2();
            this.tableTableAdapter = new bed_exit_alarm_one_window_for_test.beddbDataSet2TableAdapters.TableTableAdapter();
            this.bedTableAdapter = new bed_exit_alarm_one_window_for_test.beddbDataSet3TableAdapters.BedTableAdapter();
            this.dataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataTableBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.north = new System.Windows.Forms.TabPage();
            this.west = new System.Windows.Forms.TabPage();
            this.south = new System.Windows.Forms.TabPage();
            this.east = new System.Windows.Forms.TabPage();
            this.dirction = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.bedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beddbDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beddbDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTableBindingSource1)).BeginInit();
            this.dirction.SuspendLayout();
            this.SuspendLayout();
            // 
            // bedBindingSource
            // 
            this.bedBindingSource.DataMember = "Bed";
            this.bedBindingSource.DataSource = this.beddbDataSet3;
            // 
            // beddbDataSet3
            // 
            this.beddbDataSet3.DataSetName = "beddbDataSet3";
            this.beddbDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // beddbDataSet2
            // 
            this.beddbDataSet2.DataSetName = "beddbDataSet2";
            this.beddbDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableTableAdapter
            // 
            this.tableTableAdapter.ClearBeforeFill = true;
            // 
            // bedTableAdapter
            // 
            this.bedTableAdapter.ClearBeforeFill = true;
            // 
            // dataTableBindingSource
            // 
            this.dataTableBindingSource.DataSource = typeof(System.Data.DataTable);
            // 
            // dataTableBindingSource1
            // 
            this.dataTableBindingSource1.DataSource = typeof(System.Data.DataTable);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "BEA_UI0820-10.png");
            // 
            // north
            // 
            this.north.AutoScroll = true;
            this.north.Location = new System.Drawing.Point(4, 25);
            this.north.Name = "north";
            this.north.Padding = new System.Windows.Forms.Padding(3);
            this.north.Size = new System.Drawing.Size(691, 549);
            this.north.TabIndex = 3;
            this.north.Text = "北";
            // 
            // west
            // 
            this.west.AutoScroll = true;
            this.west.Location = new System.Drawing.Point(4, 25);
            this.west.Name = "west";
            this.west.Padding = new System.Windows.Forms.Padding(3);
            this.west.Size = new System.Drawing.Size(691, 549);
            this.west.TabIndex = 2;
            this.west.Text = "西";
            // 
            // south
            // 
            this.south.AutoScroll = true;
            this.south.Location = new System.Drawing.Point(4, 25);
            this.south.Name = "south";
            this.south.Padding = new System.Windows.Forms.Padding(3);
            this.south.Size = new System.Drawing.Size(691, 549);
            this.south.TabIndex = 1;
            this.south.Text = "南";
            // 
            // east
            // 
            this.east.AutoScroll = true;
            this.east.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.east.Location = new System.Drawing.Point(4, 25);
            this.east.Name = "east";
            this.east.Padding = new System.Windows.Forms.Padding(3);
            this.east.Size = new System.Drawing.Size(691, 549);
            this.east.TabIndex = 0;
            this.east.Text = "東";
            // 
            // dirction
            // 
            this.dirction.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dirction.Controls.Add(this.east);
            this.dirction.Controls.Add(this.south);
            this.dirction.Controls.Add(this.west);
            this.dirction.Controls.Add(this.north);
            this.dirction.HotTrack = true;
            this.dirction.Location = new System.Drawing.Point(-2, -1);
            this.dirction.Name = "dirction";
            this.dirction.SelectedIndex = 0;
            this.dirction.Size = new System.Drawing.Size(699, 578);
            this.dirction.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.dirction.TabIndex = 0;
            this.dirction.Click += new System.EventHandler(this.dirction_Click);
            // 
            // bed_exit_alarm_system
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 574);
            this.Controls.Add(this.dirction);
            this.KeyPreview = true;
            this.Name = "bed_exit_alarm_system";
            this.Text = "離床警報器";
            this.Load += new System.EventHandler(this.bed_exit_alarm_system_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bed_exit_alarm_system_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.bedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beddbDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beddbDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTableBindingSource1)).EndInit();
            this.dirction.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private beddbDataSet2 beddbDataSet2;
        private beddbDataSet2TableAdapters.TableTableAdapter tableTableAdapter;
        private beddbDataSet3 beddbDataSet3;
        private System.Windows.Forms.BindingSource bedBindingSource;
        private beddbDataSet3TableAdapters.BedTableAdapter bedTableAdapter;
        private System.Windows.Forms.BindingSource dataTableBindingSource;
        private System.Windows.Forms.BindingSource dataTableBindingSource1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabPage north;
        private System.Windows.Forms.TabPage west;
        private System.Windows.Forms.TabPage south;
        private System.Windows.Forms.TabPage east;
        private System.Windows.Forms.TabControl dirction;
    }
}

