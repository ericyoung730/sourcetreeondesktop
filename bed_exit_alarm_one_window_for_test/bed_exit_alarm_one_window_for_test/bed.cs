﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace bed_exit_alarm_one_window_for_test
{
    public partial class Bed : Form
    {
        // 宣告需要的物件
        public Label name_label;       // 裝名字的 label，一開始預設的內容為 bed_01...
        public Label connect;          // 顯現連線狀態
        private Button change_name;     // 按鈕，用於更改名字
        private Button Reset_btn;       // 用來重置燈號與 last_status
        public PictureBox sign_US;     // 超聲波的燈號
        public PictureBox sign_IR;     // 紅外線的燈號
        public PictureBox sign_BAR;     // 欄杆的燈號
        public PictureBox sign_both;   // 離床警示的燈號。當欄杆放下時會直接變為紅燈
        public PictureBox sign_battery;     // 電池燈號
        public PictureBox sign_wifi;   // 斷線燈號
        private Label sign_US_tag;    // 超聲波燈號底下的標籤
        private Label sign_IR_tag;    // 紅外線燈號底下的標籤
        private Label sign_BAR_tag;    // 欄杆燈號底下的標籤
        private Label sign_both_tag;  // 離床警示的燈號底下的標籤
        public DateTime time = new DateTime();       //存取時間
        public int select = 100;           // 儲存選取編號
        public int order;            // 表示該物件在 Panel中由上到下的順序，最小是 0
        public int ln33_tag;         // 紀錄該物件對應的裝置地址
        public int battery = 0;
        public int status_us = 0;
        public int status_ir = 0;
        public int status_rail = 0;
        // 宣告物件的尺寸參數
        private int left_edge_offset = 10;      // 與左邊界的距離
        private int top_edge_offset = 10;       // 與上邊界的距離
        private int bed_obj_height = 200;       // 物件的高度
        private int sign_width = 90;            // 燈號的寬度
        private int sign_height = 80;           // 燈號的高度
        private int sign_sign_space = 40;       // 兩個燈號之間的間距
        private int name_label_height = 38;     // name_label的高度
        private int name_label_width = 130;      // name_label的寬度
        private int connect_width = 100;      // name_label的寬度
        private int label_sign_space = 20;      // name_label跟燈號之間的間距
        private int tag_height = 30;            // 燈號標籤的高度
        private int tag_width = 90;             // 燈號標籤的長度
        private int sign_tag_space = 10;        // 燈號與燈號標籤間的間距 
        public delegate void d();
        public Bed(Panel pan, int o, int d)
        {
            // 建立物件
            order = o;
            ln33_tag = d;
            name_label = new Label()
            {
                // 初始化 name_label
                Text = "bed_" + d.ToString(),
                Left = left_edge_offset,
                Top = top_edge_offset + bed_obj_height * order,
                Height = name_label_height,
                Width = name_label_width,
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                Font = new Font("微軟正黑體", 12, FontStyle.Bold),
            };
            connect = new Label()
            {
                // 初始化 name_label
                Text = "未連線",
                Left = left_edge_offset + 375,
                Top = top_edge_offset + bed_obj_height * order,
                Height = name_label_height,
                Width = connect_width,
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                Font = new Font("微軟正黑體", 12, FontStyle.Bold),
            };
            change_name = new Button()
            {
                // 初始化 change_name
                Text = "改變名字",
                Left = left_edge_offset + name_label_width,
                Top = top_edge_offset + bed_obj_height * order,
                Height = name_label_height,
                Width = 100,
                Font = new Font("微軟正黑體", 12, FontStyle.Bold),
                AutoSize = false,
            };
            Reset_btn = new Button()
            {
                // 初始化 reset button
                Text = "Reset",
                Left = left_edge_offset + name_label_width + 120,
                Top = top_edge_offset + bed_obj_height * order,
                Height = name_label_height,
                Width = 80,
                Font = new Font("新細明體", 12, FontStyle.Bold),
                AutoSize = false,
            };
            /*Select_btn = new Button()
            {
                // 初始化 reset button
                Text = "選取",
                Left = left_edge_offset + name_label_width + 240,
                Top = top_edge_offset + bed_obj_height * order,
                Height = name_label_height,
                Width = 80,
                Font = new Font("新細明體", 12, FontStyle.Bold),
                AutoSize = false,
            };*/
            sign_US = new PictureBox()
            {
                // 初始化 US燈號
                SizeMode = PictureBoxSizeMode.StretchImage,
                Image = Properties.Resources.BEA_UI0820_13,
                Height = sign_height,
                Width = sign_width,
                Left = left_edge_offset + 4,
                Top = top_edge_offset + name_label_height + label_sign_space + bed_obj_height * order,
            };
            sign_IR = new PictureBox()
            {
                // 初始化 IR燈號
                SizeMode = PictureBoxSizeMode.StretchImage,
                Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13,
                Height = sign_height,
                Width = sign_width,
                Left = left_edge_offset + sign_width + sign_sign_space + 4,
                Top = top_edge_offset + name_label_height + label_sign_space + bed_obj_height * order,
            };
            sign_BAR = new PictureBox()
            {
                // 初始化 BAR燈號
                SizeMode = PictureBoxSizeMode.StretchImage,
                Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13,
                Height = sign_height,
                Width = sign_width,
                Left = left_edge_offset + sign_width * 2 + sign_sign_space * 2,
                Top = top_edge_offset + name_label_height + label_sign_space + bed_obj_height * order,
            };
            sign_both = new PictureBox()
            {
                // 初始化 both燈號
                SizeMode = PictureBoxSizeMode.StretchImage,
                Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13,
                Height = sign_height,
                Width = sign_width,
                Left = left_edge_offset + sign_width * 3 + sign_sign_space * 3 - 4,
                Top = top_edge_offset + name_label_height + label_sign_space + bed_obj_height * order,
            };
            sign_battery = new PictureBox()
            {
                // 初始化 both燈號
                Image = bed_exit_alarm_one_window_for_test.Properties.Resources.battery,
                Height = 26,
                Width = 50,
                Left = left_edge_offset + sign_width * 3 + sign_sign_space * 5 + 10,
                Top = top_edge_offset + label_sign_space + bed_obj_height * order + 40,
                Visible = false
            };
            sign_wifi = new PictureBox()
            {
                // 初始化 both燈號
                Image = bed_exit_alarm_one_window_for_test.Properties.Resources.wifi,
                Height = 40,
                Width = 40,
                Left = left_edge_offset + sign_width * 3 + sign_sign_space * 5 + 10,
                Top = top_edge_offset + name_label_height + bed_obj_height * order + 80,
                Visible = false
            };
            sign_US_tag = new Label()
            {
                // 初始化燈號標籤
                Text = "超音波",
                Height = tag_height,
                Width = tag_width,
                Left = left_edge_offset + 2,
                Top = top_edge_offset + name_label_height + sign_height + sign_tag_space + label_sign_space + bed_obj_height * order,
                AutoSize = false,
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                Font = new Font("微軟正黑體", 12, FontStyle.Bold),
            };
            sign_IR_tag = new Label()
            {
                // 初始化燈號標籤
                Text = "紅外線",
                Height = tag_height,
                Width = tag_width,
                Left = left_edge_offset + tag_width + sign_sign_space + 2,
                Top = top_edge_offset + name_label_height + sign_height + sign_tag_space + label_sign_space + bed_obj_height * order,
                AutoSize = false,
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                Font = new Font("微軟正黑體", 12, FontStyle.Bold),
            };
            sign_BAR_tag = new Label()
            {
                // 初始化燈號標籤
                Text = "跨欄",
                Height = tag_height,
                Width = tag_width,
                Left = left_edge_offset + tag_width * 2 + sign_sign_space * 2,
                Top = top_edge_offset + name_label_height + sign_height + sign_tag_space + label_sign_space + bed_obj_height * order,
                AutoSize = false,
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                Font = new Font("微軟正黑體", 12, FontStyle.Bold),
            };
            sign_both_tag = new Label()
            {
                // 初始化燈號標籤
                Text = "離床",
                Height = tag_height,
                Width = tag_width,
                Left = left_edge_offset + tag_width * 3 + sign_sign_space * 3 - 4,
                Top = top_edge_offset + name_label_height + sign_height + sign_tag_space + label_sign_space + bed_obj_height * order,
                AutoSize = false,
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                Font = new Font("微軟正黑體", 12, FontStyle.Bold),
            };
            // 將動態物件放入 panel
            pan.Controls.Add(name_label);
          //  pan.Controls.Add(connect);
            pan.Controls.Add(change_name);
            pan.Controls.Add(Reset_btn);
            pan.Controls.Add(sign_US);
            pan.Controls.Add(sign_IR);
            pan.Controls.Add(sign_BAR);
            pan.Controls.Add(sign_both);
           // pan.Controls.Add(sign_battery);
           // pan.Controls.Add(sign_wifi);
            pan.Controls.Add(sign_US_tag);
            pan.Controls.Add(sign_IR_tag);
            pan.Controls.Add(sign_BAR_tag);
            pan.Controls.Add(sign_both_tag);
            // 將按鈕加入事件
            change_name.Click += new EventHandler(change_name_event);
            Reset_btn.Click += new EventHandler(reset_btn_click);
            // Select_btn.Click += new EventHandler(Select_btn_event);
        }
        public void remove_from_panel(Panel pan)
        {
            pan.Controls.Remove(name_label);
            pan.Controls.Remove(connect);
            pan.Controls.Remove(change_name);
            pan.Controls.Remove(Reset_btn);
            pan.Controls.Remove(sign_US);
            pan.Controls.Remove(sign_IR);
            pan.Controls.Remove(sign_BAR);
            pan.Controls.Remove(sign_both);
            pan.Controls.Remove(sign_US_tag);
            pan.Controls.Remove(sign_IR_tag);
            pan.Controls.Remove(sign_BAR_tag);
            pan.Controls.Remove(sign_both_tag);
        }
        public void bed_newLocation()
        {
            // 將該物件上移一格
            order--;
            name_label.Top = top_edge_offset + bed_obj_height * order;
            change_name.Top = top_edge_offset + bed_obj_height * order;
            connect.Top = top_edge_offset + bed_obj_height * order;
            Reset_btn.Top = top_edge_offset + bed_obj_height * order;
            sign_US.Top = top_edge_offset + name_label_height + label_sign_space + bed_obj_height * order;
            sign_IR.Top = top_edge_offset + name_label_height + label_sign_space + bed_obj_height * order;
            sign_BAR.Top = top_edge_offset + name_label_height + label_sign_space + bed_obj_height * order;
            sign_both.Top = top_edge_offset + name_label_height + label_sign_space + bed_obj_height * order;
            sign_US_tag.Top = top_edge_offset + name_label_height + sign_height + sign_tag_space + label_sign_space + bed_obj_height * order;
            sign_IR_tag.Top = top_edge_offset + name_label_height + sign_height + sign_tag_space + label_sign_space + bed_obj_height * order;
            sign_BAR_tag.Top = top_edge_offset + name_label_height + sign_height + sign_tag_space + label_sign_space + bed_obj_height * order;
            sign_both_tag.Top = top_edge_offset + name_label_height + sign_height + sign_tag_space + label_sign_space + bed_obj_height * order;
        }
        private void SetReturnValueCallbackFun(String str)
        {
            name_label.Text = str;
        }
        public void change_name_event(Object sender, EventArgs e)
        {
            // 跳出視窗，等待輸入文字
            name_Form n = new name_Form();
            n.ReturnValueCallback += new name_Form.ReturnValueDelegate(this.SetReturnValueCallbackFun);
            n.Show();
            // 刪除 name_Form物件
            n = null;
        }
        public void display_US_Save()
        {
            sign_US.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13;
        }
        public void display_IR_Save()
        {
            sign_IR.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13;
        }
        public void display_BAR_Save()
        {
            sign_BAR.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13;
        }
        public void display_both_Save()
        {
            sign_both.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13;
        }
        public void display_US_Warning()
        {
            sign_US.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_14;
        }
        public void display_IR_Warning()
        {
            sign_IR.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_14;
        }
        public void display_BAR_Warning()
        {
            sign_BAR.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_14;
        }
        public void display_both_Warning()
        {
            sign_both.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_14;
        }
        public void ch(String s)
        {
            name_label.Text = s;
        }
        public void reset_sign()
        {
            sign_US.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13;
            sign_IR.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13;
            sign_BAR.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13;
            sign_both.Image = bed_exit_alarm_one_window_for_test.Properties.Resources.BEA_UI0820_13;
            status_ir = 0;
            status_rail = 0;
            status_us = 0;
        }
        public void reset_btn_click(Object sender, EventArgs e)
        {
            reset_sign();
        }

        private void bed_Load(object sender, EventArgs e)
        {

        }

    }
    public class name_Form : Form
    {
        public TextBox input_name;
        public Button enter_button;

        public name_Form()
        {
            this.Height = 100;
            this.Width = 250;
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            // 宣告 輸入欄位 與 確定按鈕
            input_name = new TextBox()
            {
                Left = 10,
                Top = 10,
                Height = 38,
                Width = 120,
                Text = "",
            };
            enter_button = new Button()
            {
                Left = 10 + 120,
                Top = 10,
                Height = 24,
                Width = 70,
                Text = "確定",
            };

            enter_button.Click += new EventHandler(enter_name_event);
            input_name.KeyDown += new KeyEventHandler(input_name_KeyDown);
            this.Controls.Add(input_name);
            this.Controls.Add(enter_button);

        }

        private void input_name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                enter_button.Focus();

                enter_button_Click(sender, e);



            }
        }

        private void enter_button_Click(object sender, KeyEventArgs e)
        {
            ReturnValueCallback(input_name.Text);
            this.Close();
        }
        public delegate void ReturnValueDelegate(String str);
        public event ReturnValueDelegate ReturnValueCallback;
        public void enter_name_event(Object sender, EventArgs e)
        {
            ReturnValueCallback(input_name.Text);
            this.Close();
        }
    }
}
