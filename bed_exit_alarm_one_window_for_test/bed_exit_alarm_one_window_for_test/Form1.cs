﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Management;
using System.IO.Ports;
using System.Threading;
using System.Configuration;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Net;
using System.Web;
using System.Data;
using System.IO;
using CsvHelper;
using System.Text;
using System.Drawing;

namespace bed_exit_alarm_one_window_for_test
{
    public partial class bed_exit_alarm_system : Form
    {
        public SerialPort serialport;
        public List<int> buffer = new List<int>();
        private DateTime universal_time;
        DateTime myDateTime = DateTime.Now;
        private Boolean receiving;
        private int data_head = 0xFE;
        private int data_tail = 0xFF;
        public Bed_panel bedpanel_east;
        public Bed_panel bedpanel_south;
        public Bed_panel bedpanel_west;
        public Bed_panel bedpanel_north;
        private Thread t;
        private delegate void del_text(int name, int us, int ir, int bar, int battery, int con, int dir);
        private delegate void del_csvwrite(int name, int us, int ir, int bar, int battery, int con, int dir);
        private delegate void del_thingspeaktext(int name, int us, int ir, int bar, int battery, int con, int dir);
        private delegate void del_upgrade(int nRowIndex);
        private delegate int del_add_bed(int int1, int int2);
        private delegate void del_sign_changing(int i);
        private delegate void del_connect_change(int i, Boolean receiving);
        private delegate void del_text_change(int i, int battery);
        private delegate void del_connect_time(int i);
        private delegate double del_timecompare(int i, DateTime time);
        SqlConnection connection;
        string connectionString;
        private DataTable my_datatable;
        string path = Path.Combine(Directory.GetCurrentDirectory(), "data.csv");
        private const string WRITEKEY = "D1OUP1QB81RE3UW4";
        private const string strUpdateBase = "http://api.thingspeak.com/update";
        private string strUpdateURI = strUpdateBase + "?key=" + WRITEKEY;
        double td;
        int type_data = 0x0;
        int type_ins = 0x1;
        public bool serial_open = false;
        public bed_exit_alarm_system()
        {
            InitializeComponent();
            bedpanel_east = new Bed_panel(this);
            bedpanel_south = new Bed_panel(this);
            bedpanel_west = new Bed_panel(this);
            bedpanel_north = new Bed_panel(this);

        }
        private String Find_friendly_name_CP2012()
        {
            // 利用 freindly name尋找有插 uart的 com port編號
            // 要尋找的字串
            const string str = "Silicon Labs CP210x USB to UART Bridge";
            // 建立要搜尋的物件
            ManagementObjectCollection mbslist = null;
            ManagementObjectSearcher mbs = new ManagementObjectSearcher("select * from Win32_SerialPort");
            mbslist = mbs.Get();

            // 執行搜尋的動作，取出 "comx"的字串
            string str_comport = "";
            foreach (ManagementObject mo in mbslist)
            {

                if (string.Compare(mo["Name"].ToString(), 0, str, 0, 38) == 0) // 如果找到的話
                {
                    str_comport = mo["Name"].ToString();
                    // 將字串取出，儲存在 str_comport
                    str_comport = str_comport.Remove(0, str_comport.IndexOf("(") + 1);
                    str_comport = str_comport.Remove(str_comport.Length - 1);
                    break;
                }
            }
            return str_comport;
        }
        private String Find_friendly_name_PL2303()
        {
            // 利用 freindly name尋找有插 uart的 com port編號
            // 要尋找的字串
            const string str = "Prolific USB-to-Serial Comm Port";
            // 建立要搜尋的物件
            ManagementObjectCollection mbslist = null;
            ManagementObjectSearcher mbs = new ManagementObjectSearcher("select * from Win32_PnPEntity");
            mbslist = mbs.Get();

            // 執行搜尋的動作，取出 "comx"的字串
            string str_comport = "";
            foreach (ManagementObject mo in mbslist)
            {

                if (string.Compare(mo["Name"].ToString(), 0, str, 0, 32) == 0) // 如果找到的話
                {
                    str_comport = mo["Name"].ToString();
                    // 將字串取出，儲存在 str_comport
                    str_comport = str_comport.Remove(0, str_comport.IndexOf("(") + 1);
                    str_comport = str_comport.Remove(str_comport.Length - 1);
                    break;
                }
            }
            return str_comport;
        }
        public bool IsNotExistsOrRecordEmpty(string file)
        {
            if (!File.Exists(file))
            {
                return true;
            }
            return false;

        }
        private void bed_exit_alarm_system_Load(object sender, EventArgs e)
        {
            del_add_bed d = bedpanel_east.Add_bed;
            Invoke(d, 00, 00);
            my_datatable = new DataTable();
            //dataGridView1.DataSource = my_datatable;
            // TODO: 這行程式碼會將資料載入 'beddbDataSet3.Bed' 資料表。您可以視需要進行移動或移除。
            // this.bedTableAdapter.Fill(this.beddbDataSet3.Bed);
            //  connectionString = ConfigurationManager.ConnectionStrings["bed_exit_alarm_one_window_for_test.Properties.Settings.beddbConnectionString"].ConnectionString;
            east.Controls.Add(bedpanel_east);
            south.Controls.Add(bedpanel_south);
            west.Controls.Add(bedpanel_west);
            north.Controls.Add(bedpanel_north);
            string str_comport = Find_friendly_name_CP2012();//儲存物件名稱
            //string str_comport = Find_friendly_name_PL2303();//儲存物件名稱
            if (IsNotExistsOrRecordEmpty(path))
            {
                using (StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Create, FileAccess.Write)))
                {
                    writer.WriteLine("name,Us,Ir,bar,direction,time,other");
                }
            }
            else
            {
                string[] raw_data = System.IO.File.ReadAllLines(path);
                string[] data_col = null;
                int x = 0;
                foreach (string line in raw_data)
                {
                    data_col = line.Split(',');
                    if (x == 0)
                    {
                        for (int i = 0; i < data_col.Length; i++)
                        {
                            /* if (i == 1)
                             {
                                 my_datatable.Columns.Add(data_col[i], typeof(CheckBox));
                             }
                             else if(i==2)
                             {
                                 my_datatable.Columns.Add(data_col[i], typeof(CheckBox));
                             }
                             else if (i == 3)
                             {
                                 my_datatable.Columns.Add(data_col[i], typeof(CheckBox));
                             }
                             else
                             {
                                 my_datatable.Columns.Add(data_col[i]);
                             }*/
                            my_datatable.Columns.Add(data_col[i]);
                        }
                        x++;
                    }
                    else
                    {
                        /*string True = ("1");
                        string False = ("0");
                        object[] array_col = new object[7];
                        array_col = data_col;

                        for (int i = 1; i < 4;i++)
                        {
                            if(array_col[i].Equals(True))
                            {
                                array_col[i] = true;
                            }
                            else if (array_col[i] .Equals(False))
                            {
                               array_col[i]= false;
                            }
                            
                        }*/
                        my_datatable.Rows.Add(data_col);
                    }
                }
            }
            try
            {
                int baud = 115200;//胞率
                int serial_data_size = 8;//data大小
                if (!serial_open)//如果還沒創建serial
                {
                    serialport = new SerialPort(str_comport, baud, Parity.None, serial_data_size, StopBits.One);//創建
                }
                serialport.Open();
                // 打開 serial port
                Threading();
            }
            catch (System.ArgumentException)
            {
                MessageBox.Show("未正確插入裝置，請再確認。\n請關閉程式，插入裝置後再重新開啟程式");
                // System.Windows.Forms.Application.Exit();
            }
        }
        private void Threading()
        {
            t = new Thread(DoReceive)//此執行緒所呼叫之fuction
            {
                IsBackground = true //設定為背景執行緒
            };

            t.Start();//開始執行
        }
        public void change_light(Bed_panel bedpanel, int i)
        {
            del_connect_change change;
            del_connect_time time;
            del_timecompare compare;
            del_text_change text;
            del_sign_changing d;
            if (buffer[5] == type_data)
            {
                d = bedpanel.railzero;
                this.Invoke(d, i);
                if (i >= 0)
                {
                    if (buffer[7] == 0x10 && buffer[8] == 0x00)
                    {
                        d = bedpanel.US_safe;
                        this.Invoke(d, i);
                        //status_us = 0;
                    }
                    if (buffer[7] == 0x01 && buffer[8] == 0x00)
                    {
                        d = bedpanel.US_warning;
                        this.Invoke(d, i);
                        //status_us = 1;
                    }
                    if (buffer[7] == 0x10 && buffer[8] == 0x01)
                    {
                        d = bedpanel.IR_Warning;
                        this.Invoke(d, i);
                        //status_ir = 1;
                    }
                    if (buffer[7] == 0x10 && buffer[8] == 0x10)
                    {

                        d = bedpanel.IR_safe;
                        this.Invoke(d, i);
                        //status_ir = 0;
                    }
                    if (buffer[7] == 0x11 && buffer[8] == 0x01)
                    {
                        d = bedpanel.BAR_Warning;
                        this.Invoke(d, i);
                        // status_rail = 1;
                    }
                    d = bedpanel.check;
                    this.Invoke(d, i);
                    if (buffer[7] == 0x00 && buffer[8] == 0x00)
                    {
                        d = bedpanel.batteryone;
                        this.Invoke(d, i);
                        text = bedpanel.Change_text_status;
                        this.Invoke(text, i, 0);
                    }
                    if (buffer[7] == 0x00 && buffer[8] == 0x01)
                    {
                        d = bedpanel.batterytwo;
                        this.Invoke(d, i);
                        text = bedpanel.Change_text_status;
                        this.Invoke(text, i, 0);
                    }
                }
                time = bedpanel.Connect_time;
                this.Invoke(time, i);
                change = bedpanel.Change_connect_status;
                this.Invoke(change, i, receiving);
            }
            else if (buffer[5] == type_ins)
            {
                time = bedpanel.Connect_time;
                this.Invoke(time, i);
                change = bedpanel.Change_connect_status;
                this.Invoke(change, i, receiving);

            }
            compare = bedpanel.Timecompare;
            td = compare(i, universal_time);
            if (td > 3)
            {
                receiving = false;
                change = bedpanel.Change_connect_status;
                this.Invoke(change, i, receiving);
            }
        }
        public void compare_else(Bed_panel bedpanel, int i)
        {
            del_connect_change change;
            del_timecompare compare;
            if (bedpanel.order_ptr > 0)
            {
                while (i < bedpanel.order_ptr)
                {
                    compare = bedpanel.Timecompare;
                    td = compare(i, universal_time);
                    if (td > 5)
                    {
                        receiving = false;
                        change = bedpanel.Change_connect_status;
                        this.Invoke(change, i, receiving);
                    }
                    i++;
                }
            }
        }
        private void DoReceive()
        {
            while (true)
            {
                universal_time = DateTime.Now;
                try
                {
                    //battery = 0;
                    int t = 0;
                    int z = serialport.BytesToRead;
                    if (z != 0)
                    {
                        if (serialport.ReadByte() == data_head)
                        {
                            receiving = true;
                            while (receiving)
                            {
                                buffer.Add(serialport.ReadByte());
                                if (buffer[t] == data_tail)
                                    break;
                                t++;
                            }

                            int dir = 0, us = 0, ir = 0, bar = 0, bat = 0, con = 0;

                            if (buffer[5] == 0x0)
                            {
                                if (buffer[6] == 0x00)
                                {
                                    dir = 0;
                                }
                                if (buffer[6] == 0x01)
                                {
                                    dir = 1;
                                }
                                if (buffer[6] == 0x10)
                                {
                                    dir = 2;
                                }
                                if (buffer[6] == 0x11)
                                {
                                    dir = 3;
                                }
                                if (buffer[7] == 0x01 && buffer[8] == 0x00)
                                {
                                    us = 1;
                                }
                                if (buffer[7] == 0x10 && buffer[8] == 0x01)
                                {
                                    ir = 1;
                                }
                                if (buffer[7] == 0x11 && buffer[8] == 0x01)
                                {
                                    bar = 1;
                                }
                                if (buffer[7] == 0x00 && buffer[8] == 0x00)
                                {
                                    bat = 1;
                                }
                                if (buffer[7] == 0x00 && buffer[8] == 0x01)
                                {
                                    con = 1;
                                }
                                int name = buffer[4] * 256 + buffer[3];
                                /*   del_text d;
                                   d = text;
                               this.Invoke(d, name, us, ir, bar, bat, con, dir);*/
                                //del_csvwrite csv;
                               // csv = csvwrite;
                               // this.Invoke(csv, name, us, ir, bar, bat, con, dir);
                               // del_thingspeaktext iot;
                               // iot = thingspeaktext;
                                //this.Invoke(iot, name, us, ir, bar, bat, con, dir);

                                /*dataGridView1.ClearSelection();
                                int nRowIndex = dataGridView1.Rows.Count - 1;
                                int nColumnIndex = 3;
                                dataGridView1.Rows[nRowIndex].Selected = true;
                                dataGridView1.Rows[nRowIndex].Cells[nColumnIndex].Selected = true;
                                del_upgrade u;
                                u = upgrade;
                                this.Invoke(u, nRowIndex);*/
                            }
                            us = 0;
                            ir = 0;
                            bar = 0;
                            bat = 0;
                            con = 0;
                            if (buffer[6] == 0x00)
                            {
                                bedpanel_east.i = bedpanel_east.Find_tag(buffer[3], buffer[4]);
                                del_add_bed d = bedpanel_east.Add_bed;
                                change_light(bedpanel_east, bedpanel_east.i);
                                if ((int)Invoke(d, buffer[3], buffer[4]) == 0) // 若成功產生物件，則回傳
                                {

                                }
                            }
                            else if (buffer[6] == 0x01)
                            {
                                bedpanel_south.i = bedpanel_south.Find_tag(buffer[3], buffer[4]);
                                del_add_bed d = bedpanel_south.Add_bed;
                                change_light(bedpanel_south, bedpanel_south.i);
                                if ((int)Invoke(d, buffer[3], buffer[4]) == 0) // 若成功產生物件，則回傳
                                {

                                }
                            }
                            else if (buffer[6] == 0x10)
                            {
                                bedpanel_west.i = bedpanel_west.Find_tag(buffer[3], buffer[4]);
                                del_add_bed d = bedpanel_west.Add_bed;
                                change_light(bedpanel_west, bedpanel_west.i);
                                if ((int)Invoke(d, buffer[3], buffer[4]) == 0) // 若成功產生物件，則回傳
                                {

                                }
                            }
                            else if (buffer[6] == 0x11)
                            {
                                bedpanel_north.i = bedpanel_north.Find_tag(buffer[3], buffer[4]);
                                del_add_bed d = bedpanel_north.Add_bed;
                                change_light(bedpanel_north, bedpanel_north.i);
                                if ((int)Invoke(d, buffer[3], buffer[4]) == 0) // 若成功產生物件，則回傳
                                {

                                }
                            }
                        }
                    }
                    compare_else(bedpanel_east, 0);
                    compare_else(bedpanel_south, 0);
                    compare_else(bedpanel_west, 0);
                    compare_else(bedpanel_north, 0);
                    buffer.Clear();
                    Thread.Sleep(16);
                }
                catch (Exception)
                {
                    MessageBox.Show("資料量過大，接收失敗");
                }
            }
        }
        private void upgrade(int nRowIndex)
        {
            //dataGridView1.FirstDisplayedScrollingRowIndex = nRowIndex;
        }
        private void text(int name, int us, int ir, int bar, int battery, int con, int dir)
        {
            char direction = ' ';
            string other = " ";
            string query = "INSERT INTO Bed VALUES (@BedName,@BedUr,@BedIr,@BedBar,@BedTime,@Beddirction,@Bedothers)";
            if (dir == 0)
            {
                direction = '東';
            }
            else if (dir == 1)
            {
                direction = '南';
            }
            else if (dir == 2)
            {
                direction = '西';
            }
            else if (dir == 3)
            {
                direction = '北';
            }
            if (battery == 1)
            {
                other = "沒電";
            }
            if (con == 1)
            {
                other = "斷線";

            }
            using (connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@BedName", name);
                command.Parameters.AddWithValue("@BedUr", us);
                command.Parameters.AddWithValue("@BedIr", ir);
                command.Parameters.AddWithValue("@BedBar", bar);
                command.Parameters.AddWithValue("@Beddirction", direction);
                command.Parameters.AddWithValue("@BedTime", myDateTime);
                command.Parameters.AddWithValue("@Bedothers", other);
                command.ExecuteScalar();
            }
            this.bedTableAdapter.Fill(this.beddbDataSet3.Bed);
        }
        private void csvwrite(int name, int us, int ir, int bar, int battery, int con, int dir)
        {
            myDateTime = DateTime.Now;
            char direction = ' ';
            string other = " ";
            if (dir == 0)
            {
                direction = '東';
            }
            else if (dir == 1)
            {
                direction = '南';
            }
            else if (dir == 2)
            {
                direction = '西';
            }
            else if (dir == 3)
            {
                direction = '北';
            }
            if (battery == 1)
            {
                other = "沒電";
            }
            if (con == 1)
            {
                other = "斷線";

            }
            string[] data_col = null;
            string data = name.ToString() + "," + us.ToString() + "," + ir.ToString() + "," + bar.ToString() + "," + direction.ToString() + "," + myDateTime.ToString() + "," + other.ToString();
            using (StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Append, FileAccess.Write)))
            {
                data_col = data.Split(',');
                writer.WriteLine(data);
                my_datatable.Rows.Add(data_col);
            }

        }
        private void thingspeaktext(int name, int us, int ir, int bar, int battery, int con, int dir)
        {
            try
            {

                string strField1 = name.ToString();
                string strField2 = us.ToString();
                string strField3 = ir.ToString();
                string strField4 = bar.ToString();
                string strField5 = dir.ToString();
                string strField6 = "";
                if (battery == 1)
                {
                    strField6 = "1";//沒電
                }
                if (con == 1)
                {
                    strField6 = "2";//斷線
                }
                HttpWebRequest ThingsSpeakReq;
                HttpWebResponse ThingsSpeakResp;

                strUpdateURI += "&field1=" + strField1;
                strUpdateURI += "&field2=" + strField2;
                strUpdateURI += "&field3=" + strField3;
                strUpdateURI += "&field4=" + strField4;
                strUpdateURI += "&field5=" + strField5;
                strUpdateURI += "&field6=" + strField6;

                /*ThingsSpeakReq = (HttpWebRequest)WebRequest.Create(strUpdateURI);

                ThingsSpeakResp = (HttpWebResponse)ThingsSpeakReq.GetResponse();

               if (!(string.Equals(ThingsSpeakResp.StatusDescription, "OK")))
                {
                    Exception exData = new Exception(ThingsSpeakResp.StatusDescription);
                    throw exData;
                }*/

            }
            catch (Exception ex)
            {
                //lblError.InnerText = ex.Message;
                // lblError.Style.Add("display", "block");
                throw;
            }


        }
        private void bed_exit_alarm_system_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true && e.Alt == true && e.KeyCode == Keys.D)//按住組合鍵 Ctrl + Alt + D
            {
                DialogResult result = MessageBox.Show("確定要刪除全部資料???", "刪除資料", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    my_datatable.Clear();
                    /*string sqlTrunc = "TRUNCATE TABLE Bed";
                    using (connection = new SqlConnection(connectionString))
                    using (SqlCommand cmd = new SqlCommand(sqlTrunc, connection))
                    {
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                    this.bedTableAdapter.Fill(this.beddbDataSet3.Bed);*/
                }
                else { }
            }
        }
        public class Bed_panel : Panel
        {
            public int order_ptr;
            public int select = 100;
            // order_ptr : 
            // 當 panel中有 3個物件時，其值為 3 
            private int bed_obj_height = 200;
            private int bed_obj_top_offset = 12;
            public int i;
            internal Bed[] B;
            public Bed_panel(Form f)
            {
                order_ptr = 0;
                B = new Bed[100];
                this.Left = 10;
                this.Top = 12;
                this.Height = 200;
                this.Width = 550;
                f.Controls.Add(this);
            }
            public Bed_panel()
            {
            }
            public int Add_bed()
            {
                B[order_ptr] = new Bed(this, order_ptr, order_ptr);
                this.Height = (order_ptr + 1) * bed_obj_height + bed_obj_top_offset;
                order_ptr++;
                return 0;
            }
            public void Change_connect_status(int i, Boolean receiving)
            {
                if (i >= 0)
                {
                    if (receiving == true)
                        B[i].connect.Text = "已連線";
                    else
                    {
                        if (B[i] != null)
                            B[i].connect.Text = "未連線";
                    }
                }
            }
            public void Change_text_status(int i, int battery)
            {
                if (i >= 0)
                {
                    if (B[i].battery == 1)
                    {
                        if (B[i] != null)
                            B[i].sign_battery.Visible = true;
                    }
                    else if (B[i].battery == 2)
                    {
                        if (B[i] != null)
                            B[i].sign_wifi.Visible = true;
                    }
                }
            }
            public void Connect_time(int i)
            {
                if (i >= 0)
                {
                    if (B[i] != null)
                        B[i].time = DateTime.Now;
                }
            }
            public double Timecompare(int i, DateTime time)
            {
                double x = 0;
                if (i >= 0)
                {
                    if (B[i] != null)
                    {

                        TimeSpan ts;

                        ts = time.Subtract(B[i].time);
                        x = ts.TotalSeconds;
                        return x;
                    }
                }
                return x;
            }
            public int Delete_bed()
            {
                for (int i = 0; i < order_ptr; i++)
                {
                    if (B[i].select == 1)
                    {
                        select = i;
                        B[i].select = 100;
                        break;
                    }
                    else if (i == (order_ptr - 1) && B[order_ptr - 1].select == 100)
                    {
                        MessageBox.Show("you don't choose a bed");
                    }
                }
                if (select != 100)
                {
                    B[select].remove_from_panel(this);
                    B[select].Dispose();
                    for (int i = select + 1; i < order_ptr; i++)
                    {
                        B[i - 1] = B[i];
                        B[i].bed_newLocation();
                    }
                    order_ptr--;
                    select = 100;
                }
                return 0;
            }
            public int Add_bed(int address1, int address2)
            {
                // 檢查是否有重複的 address
                // -1 表示沒找到，因此產生新的
                // 若成功建立物件，回傳 0
                // 反之回傳 -1
                if (Find_tag(address1, address2) == -1)
                {
                    B[order_ptr] = new Bed(this, order_ptr, address2 * 256 + address1);
                    this.Height = (order_ptr + 1) * bed_obj_height + bed_obj_top_offset;
                    order_ptr++;
                    return 0;
                }
                return -1;
            }
            public void US_safe(int i)
            {
                B[i].display_US_Save();
                B[i].status_us = 0;
            }
            public void US_warning(int i)
            {
                B[i].display_US_Warning();
                B[i].status_us = 1;
            }
            public void IR_safe(int i)
            {
                B[i].display_IR_Save();
                B[i].status_ir = 0;
            }
            public void IR_Warning(int i)
            {
                B[i].display_IR_Warning();
                B[i].status_ir = 1;
            }
            public void BAR_safe(int i)
            {
                B[i].display_BAR_Save();
                B[i].status_rail = 0;
            }
            public void BAR_Warning(int i)
            {
                B[i].display_BAR_Warning();
                B[i].status_rail = 1;
            }
            public void Both_safe(int i)
            {
                B[i].display_both_Save();
            }
            public void Both_warning(int i)
            {
                B[i].display_both_Warning();
            }
            public void batteryone(int i)
            {
                B[i].battery = 1;
            }
            public void batterytwo(int i)
            {
                B[i].battery = 2;
            }
            public void railzero(int i)
            {
                if (i >= 0)
                    B[i].status_rail = 0;
            }
            public void check(int i)
            {
                if (B[i].status_rail == 1)
                {
                    B[i].display_both_Warning();
                }
                if (B[i].status_ir == 1 && B[i].status_us == 1)
                {
                    B[i].display_both_Warning();
                }
            }
            public void Bed_remove(int i)
            {
                // i是要刪除的裝置的 ln33-tag
                int tmp = 0;
                while (B[tmp] != null)
                {
                    if (B[tmp].ln33_tag == i) break;
                    tmp++;
                }
                // 刪除該物件
                B[tmp].remove_from_panel(this);
                B[tmp] = null;
                // 將底下的物件上移
                int k = tmp;
                k++;
                while (k < order_ptr)
                {
                    B[k].bed_newLocation();
                    k++;
                }
                // 將物件陣列由後往前挪一格
                k = tmp + 1;
                while (B[k] != null)
                {
                    B[k - 1] = B[k];
                    k++;
                }
                B[k - 1] = null;
                order_ptr--;
            }
            // 用來回傳輸入 address得到的對應的 bed在陣列中的 index
            public int Find_tag(int address1, int address2)
            {
                int i = 0;
                int tmp_tag = address1 + address2 * 256;
                while (i < order_ptr)
                {
                    if (B[i].ln33_tag == tmp_tag) return i;
                    i++;
                }
                return -1;
            }
        }
        private void dirction_Click(object sender, EventArgs e)
        {
            /*using (StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.Append, FileAccess.Write)))
             {

                 writer.WriteLine("ID,name,Us,Ir,bar,direction,time,other");
                 my_datatable.Rows.Add("ID,name,Us,Ir,bar,direction,time,other");
             }*/
        }
    }
}
