#include <SoftwareSerial.h>
#include "Timer.h"
#include "bed_exit_alarm_TX.h"
#include "timer_interrupt.h"
#include "DL_ln33.h"
#include "new_pulseIn.h"
#include "Direction_Detect.h"
bool reset_button=false;   
bool battery_state=true;   //表示有電沒電
bool us=false;             //表示超音波偵測離床或沒離床
bool Bpartexist=true;
String Bpart_data;         //data received from Bpart
int speaker=8,pin9=9,pin10=10,battery=A0; // pin set for speaker,place switch,battery detect
byte place; // present the direction  00->east 01->south 10->west 11->north 
int a=0;
Timer T;   //alarm time object
long previousMillis = 0; 
long interval = 2000;  //time gap
void serialFlush() {
  while(mys.available() > 0) {
    char t = mys.read();
  }
}   
//turn off alarm
void callback(){
  digitalWrite(speaker,LOW);
}

// check the dirction

void findplace(){ 
 int a = digitalRead(pin9);
 int b = digitalRead(pin10);
 if(a==0 && b ==0 ){
    place=0x11;
    //Serial.print("11");
 }
 else if(a==0 && b == 1){
    place=0x10;
   // Serial.print("10");
 }
 else if(a==1 && b == 0){
    place=0x01;
    //Serial.print("01");
 }
 else if(a==1 && b == 1){
    place=0x00;
   // Serial.print("00");
 } 
}

//check power
void electricity(){  
  int a = analogRead(battery);
  float val = 5.00*a/1024;
  if(val<3.8&&battery_state==true)
  {battery_state=false;
    Serial.println(" no battery");
    ln33_transmit_data(place, 0x00, 0x00);
  }
  else if(val>4&&battery_state==false)
  {battery_state=true;
    Serial.println(" battery!!");
    ln33_transmit_data(place, 0x00, 0x02);
  }
  //Serial.println(val);
}

// US alogrithm
static inline void print_Get_Direction()
{
  uint8_t tmp = Get_Direction(reset_button);
  reset_button=false;
  //Serial.println(tmp);
  switch(tmp) {
    case Direction_Undefined:
      //Serial.print(0);
      break;
    case Direction_Leave:
      if(us==false)
      {
       us=true;
       ln33_transmit_data(place,0x01,0x00);
       Serial.print(5);
      } 
      break;
    case Direction_Stay:
     if(us==true)
      {
        us=false;
        ln33_transmit_data(place,0x10,0x00);
        Serial.print(-5);
      }
      break;
    default :
      break;
  }
}

// receive data from Bpart
char get_messenger() { 
  
  byte buff[11]={0}; 
  Bpart_data="";
  while( mys.available() ) {
    for(int t=0 ; t<12 ; t++) 
    {
    buff[t] = mys.read();
    }   
  }
  if(buff[0]==0xFE) {
    for(int t=0 ; t<11 ; t++) {
      if(buff[7] == 0x1) { 
         Bpart_data="ir_leave";
     }
        else if(buff[7] == 0x2) { 
          a=0;
          if(Bpartexist==false)
          {
           Bpartexist=true;
           Bpart_data="Bpart";
          }
     }
      else if(buff[7] == 0x3) { 
        us=false;
        reset_button=true;
        Serial.println("computer");
        break;
     }
     else if(buff[8] == 0x1) {
       Bpart_data="ir_trigger";
     }
      else if(buff[9] == 0x1) { 
      Bpart_data="rail_down";
     }
    }
  }
  a++;
  //Serial.print("a:");
  //Serial.println(a);
  if(a==200)
  {
    a=0;
    ln33_transmit_data(place,0x00,0x01);
    Bpartexist=false;
  }
  serialFlush();
}

void setup() {
  Serial.begin(115200);
  mys.begin(115200);
  TIMER1_Init();
  gpio_Init();
  ln33_Serial_Init();
  ln33_channel_Init();
  ln33_ID_Init();
  ln33_Restart();
  pinMode(speaker,OUTPUT);
  pinMode(pin10,INPUT_PULLUP);
  pinMode(pin9,INPUT_PULLUP);
  pinMode(battery,INPUT);
  attachInterrupt(0, ISR_US1, FALLING);
  attachInterrupt(1, ISR_US2, FALLING);
  Serial.println("setup");
  findplace();
  /* alarm*/
  /* if(digitalRead(speaker) == LOW)
   {
     digitalWrite(speaker,HIGH);
     T.after(9200,callback);//////讓警報維持一段時間
     Serial.println("alarm1");
   }*/
   //set up && send message
  for(int i = 0; i<3; i++){
    ln33_transmit_register(place);
    delay(100); 
  }
}

void loop() {
unsigned long currentMillis = millis();
//Serial.println(Bpart_data);
//Serial.println(digitalRead(speaker));
   if(currentMillis - previousMillis > interval) 
  {
     ln33_transmit_register(place);
     previousMillis = currentMillis;
  }
  T.update();
  findplace();
  get_messenger();
  electricity();
  new_pulseIn();
  //Serial.println("I AM ");
  new_pulseIn_filter();
  print_Get_Direction();
  if(Bpart_data=="ir_trigger")
  { 
    Serial.println("send ir_trigger");
    ln33_transmit_data(place,0x10,0x01);
    if(digitalRead(speaker) == LOW)
    {
      if(us==true)
      {
        digitalWrite(speaker,HIGH);
      T.after(9200,callback);//////讓警報維持一段時間
    
      Serial.println("alarm2");
      }
     }
   }
   else if(Bpart_data=="ir_leave") 
   { 
    Serial.println("send ir_leave");
    ln33_transmit_data(place,0x10,0x10);
   }
   else if(Bpart_data=="rail_down")
   { 
    ln33_transmit_data(place, 0x11, 0x01);
    delay(1);
    Serial.println("send rali_down");
     if(digitalRead(speaker) == LOW)
     {
       digitalWrite(speaker,HIGH);
       T.after(9200,callback);//讓警報維持一段時間
       Serial.println("alarm3");
     }
   }
   else if(Bpart_data=="Bpart")
   { 
    ln33_transmit_data(place,0x00,0x02);
    delay(1);
    Serial.println("send Bpart");
   }
}




