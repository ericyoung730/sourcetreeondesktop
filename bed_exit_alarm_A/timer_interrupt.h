#define reload_value 65536-1600 //每 10ms發生一次中斷

static inline void timer_interrupt_Init() {
  /* 設置timer中斷 */
  TCCR1A = 0;
  TCCR1B = 0;
  TCCR1B |= 0x5;
  TIMSK1 &= 0x0;
}

static inline void timer_interrupt_enable() {
  TIMSK1 |= 0x1;
}

static inline void timer_interrupt_disable() {
  TIMSK1 &= 0x0;
}
